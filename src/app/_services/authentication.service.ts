import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
  constructor(private http: Http) { }
    public token: string;

  login(username: string, password: string) {
    return this.http.post('/api/login', JSON.stringify({ uid: username, upass: password }))
        .map((response: Response) => {
          // login successful if there's a jwt token in the response
          let user = response.json();
          console.log("Login: ",user);
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
              this.token = user.token;
            localStorage.setItem('currentUser', JSON.stringify(user));
            console.log("Login succesful");
            return true;
          } else {return false;}
        });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
}
