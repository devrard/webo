﻿export class User {
    uid: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    scope;
    updated_at;
}