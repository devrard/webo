import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Router} from "@angular/router";
import {ToasterModule, ToasterService,ToasterConfig} from 'angular2-toaster';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private router:Router, private toasterService: ToasterService ) {this.toasterService = toasterService;};
  public toasterconfig : ToasterConfig =
      new ToasterConfig({
        showCloseButton: true,
        tapToDismiss: false,
        timeout: 3500,
        limit:0,
        closeHtml: '<button>Close</button>'
      });

  ngOnInit() {

  }

  ngAfterViewInit(): void
  {
    this.toasterService.pop('info', 'Information', 'Homepage loaded');
  }

  public doLogin() {
    this.router.navigate(['./login']);
  }

  public test() {
  }

}
