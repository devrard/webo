import { WeboV2Page } from './app.po';

describe('webo-v2 App', function() {
  let page: WeboV2Page;

  beforeEach(() => {
    page = new WeboV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
